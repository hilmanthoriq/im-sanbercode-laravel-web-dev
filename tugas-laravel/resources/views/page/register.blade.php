@extends('layouts/main')

@section('title')
    Buat Account Baru!
@endsection

@section('sub-title')
    Sign Up Form
@endsection

@section('content')

<form action="/welcome" method="POST">
    @csrf
    <label for="firstname">First Name :</label><br>
    <input type="text" name="firstname"><br><br>

    <label for="lastname">Last Name :</label><br>
    <input type="text" name="lastname"><br><br><br>

    <label for="gender">Gender :</label><br>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="other">Other<br><br>

    <label for="nationality">Nationality:</label>
    <select name="nationality" id="">
        <option value="Indonesia">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
    </select><br><br>
    <label for="language">Language Spoken:</label> <br>
    <input type="checkbox" name="language" id="" value="Indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="language" id="" value="English">English <br>
    <input type="checkbox" name="language" id="" value="other">Other <br><br>

    <label for="bio">Bio :</label><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>

    <input type="submit" value="Submit">
</form>


@endsection
    