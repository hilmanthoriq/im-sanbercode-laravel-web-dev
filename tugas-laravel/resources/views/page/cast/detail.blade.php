@extends('layouts/main')

@section('title')
    Halaman Detail Data Pemain
@endsection
    
@section('sub-title')
    Halaman Detail Data Pemain
@endsection

@section('content')

    <h4>Nama :{{$pemain->nama}}</h4>
    <h4>Umur :{{$pemain->umur}}</h4>
    <h4>Bio  :{{$pemain->bio}}</h4>

    <a href="/cast" class="btn btn-primary mt-5">Kembali</a>

@endsection