@extends('layouts/main')

@section('title')
    Halaman Tambah Data Pemain Baru
@endsection
    
@section('sub-title')
    Halaman Tambah Data Pemain Baru
@endsection

@section('content')

    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Pemain</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Masukkan Nama Pemain">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur Pemain</label>
            <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" placeholder="Masukkan Umur Pemain">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio Pemain</label>
            <textarea name="bio" class="form-control"></textarea>
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection