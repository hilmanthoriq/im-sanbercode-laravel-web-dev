<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'] );
Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('page/table');
});

Route::get('/data-table', function(){
    return view('page/data-table');
});


// CRUD

// Create Data
Route::get('/cast/create', [CastController::class, 'create']);
    //Menyimpan data ke Database
Route::post('/cast', [CastController::class, 'store']);

// Read Data
    // menampilkan data ke halaman
Route::get('/cast', [CastController::class, 'index']);
    // menampilkan data detail pemain berdasarkan id nya
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data
    // mengedit data
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
    // update data yg diedit ke database
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);