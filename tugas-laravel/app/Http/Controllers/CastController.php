<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('page/cast/create');
    }

    public function store(Request $request){
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ],
        [
            'nama.required'=> 'Nama harus diisi!!! ( tidak boleh kosong )',
            'umur.required'=> 'Umur harus diisi tidak boleh kosong',
        ]);

        DB::table('cast')->insert([
            'nama'=> $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('/page/cast/show', ['cast' => $cast ]);
    }

    public function show($id){
        $pemain = DB::table('cast')->find($id);
        
        return view('/page/cast/detail', ['pemain' => $pemain]);
    }
    
    public function edit($id){
        $pemain = DB::table('cast')->find($id);
        
        return view('/page/cast/edit', ['pemain' => $pemain]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ],
        [
            'nama.required'=> 'Nama harus diisi!!! ( tidak boleh kosong )',
            'umur.required'=> 'Umur harus diisi tidak boleh kosong',
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update(
            [
               'nama' => $request['nama'],
               'umur' => $request['umur'],
               'bio' => $request['bio']
            ]
            );

        return redirect('/cast');
    }

    public function destroy($id) {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
