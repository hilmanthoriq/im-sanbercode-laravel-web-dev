<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page/register');
    }

    public function welcome(Request $request){
        
        $firstName = $request['firstname'];
        $lastName = $request['lastname'];

        return view('page/welcome', [
            'firstName' => $firstName,
            'lastName' => $lastName
        ]);
    }
    
}
