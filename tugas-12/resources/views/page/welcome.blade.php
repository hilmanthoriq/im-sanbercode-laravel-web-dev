<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body class="antialiased">
        <h1>SELAMAT DATANG {{$firstName}} {{$lastName}}!</h1>
        <h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>

    </body>
</html>
